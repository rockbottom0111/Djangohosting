# PROBLEM STATEMENT 1

Write a regex to extract all the numbers with orange color background
from the below text in italics (Output should be a list)

```
{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code"
:3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}
```


## Solution
```python
import re

text = '{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or anobject that implements Countable (153)"}]}'
pattern = '\":+\d+'

result = re.findall(pattern, text)

#removeing the (":) part from the elements of the list
for i in range(len(result)):
    result[i] = result[i][2:]

print(result)
```

# PROBLEM STATEMENT 2
Please create a website - which should have
two components:
Expose all the endpoints using Rest API, with proper permissions,
authentication and documentation. Please refer to the image link -
https://i.imgur.com/T0ZCO9A.png
1. Admin Facing - Where admin user can add an android app as well as
the number of points - earned by user for downloading the app. (Please
do not use the default Django Admin)
2. User Facing - where the user can see the apps added by the admin and
the points. The user should be able to see the following fields.
● Signup and Login (Feel free to use any package for the same).
● Their Name and Profile
● Points Earned.
● Tasks completed.
● Option to upload a screenshot (which must include drag and drop) for
that particular task. (Like if a user downloads a particular app), he can
send a screenshot of the open app to confirm that he has indeed
downloaded the app.

## Solution
https://gitlab.com/rockbottom0111/django-assignment/-/blob/main/README.md?ref_type=heads

https://youtu.be/3IYYBrFLRww

## Django TaskReward

Django Web Application: Task Reward System

## Overview

This project is a simple web application built using Django, Django REST Framework, and Tailwind CSS. It allows administrators to add tasks (app details) with associated points, and users can earn points by downloading the app and uploading a screenshot.

## Features

- **Task Management**: Admins can add tasks with details such as app name, link, category, subcategory, points, and logo.
- **User Authentication**: Implemented using Django REST Framework for login and authentication.
- **Drag and Drop**: Utilizes drag and drop feature for smooth uploading of screenshots.
- **Dynamic Forms**: JavaScript is used to handle dynamic forms for task creation.
- **API Endpoints**: Django REST Framework is used to create API endpoints for user login and authentication.
- **Frontend Styling**: Tailwind CSS CDN is used for frontend styling.

## ER Diagram
```markdown
+------------------+             +----------------------+             +----------------------+
|       User       |             |        Task          |             |      user_task       |
+------------------+             +----------------------+             +----------------------+
| id (PK)          |   0..*      | id (PK)              |   0..*      | id (PK)              |
| email (unique)   | <---------  | name (unique)        | <---------  | user_id (FK)         |
| fname            |             | link                 |             | task_id (FK)         |
| lname            |             | category              |             +----------------------+
| date_joined      |             | subcategory          |             
| is_active        |             | points               |   
| is_staff         |             | logo                 |
| points           |             +----------------------+
| ifLogged         |              
| token            |              
+------------------+
```

## Technologies Used

- **Backend**: Django, Django REST Framework
- **Frontend**: HTML, CSS (Tailwind CSS)
- **Database**: SQLite (default in Django)
- **JavaScript**: Vanilla JavaScript
- **API**: Django REST Framework

## Installation

1. Clone the repository: `git clone https://github.com/your/repository.git`
2. Install dependencies: `pip install -r requirements.txt`
3. Run migrations: `python manage.py migrate`
4. Run the server: `python manage.py runserver`

## Usage

1. Access the admin panel to add tasks: `http://localhost:8000/admin`
2. admin credientials is inside the urls.py file 
3. Use the provided API endpoints for user login and authentication.
4. Access the web application frontend to view and interact with tasks.
5. Download the app associated with a task and upload a screenshot to earn points.


# PROBLEM STATEMENT 3
A. Write and share a small note about your choice of system to schedule
periodic tasks (such as downloading a list of ISINs every 24 hours). Why did
you choose it? Is it reliable enough; Or will it scale? If not, what are the
problems with it? And, what else would you recommend to fix this problem at
scale in production?

## Answer
To schedule a periodic tasks I prefer Cronjob 

Cron is a time-based job scheduler in UNIX-like operating systems. It allows you to schedule commands or scripts to run at specific times or intervals. Cron jobs are a great way to free up your time and save money, It’s reliable for basic scheduling needs and majorly supported across UNIX-like operating systems.

it will not scale well for a very large complex task schedules because Cron is very much simple and doesn't have advance features like dependency management or dynamic scheduling. Since its a python library , it integrates seamlessly with python codebase and workflows

I recommend to use more robust job scheduling systems like Apache Airflow and kubernets Cronjobs. These system offer more features better suited to handle complex task in production


B. In what circumstances would you use Flask instead of Django and vice
versa?

## Answer
I choose Flask when i have to create web applications which are small scale or medium  having  few features and pages as Flask is light weight and Simple. 
Flask also provide flexibility of choosing the components and project structure according to my need. 
on the other hand Django is a complete Framework with built-in components suited for rapid development and can handle complex projects. I even saw that flask is not very well maintained as many flask plugins are no longer maintained while django’s community is wide spread with extensive documentations


## License

This project is licensed under the [MIT License](LICENSE).
